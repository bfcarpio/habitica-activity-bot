module gitlab.com/bfcarpio/habitica-activity-bot

go 1.12

require (
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23
	github.com/namsral/flag v1.7.4-pre
	github.com/onsi/gomega v1.5.0
)
