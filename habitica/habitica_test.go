package habitica

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	. "github.com/onsi/gomega"
)

func TestValidation_RequiredArgs(t *testing.T) {
	RegisterTestingT(t)
	_, err := New("", "some-api-token")
	Expect(err).To(HaveOccurred())

	_, err = New("some-user-id", "")
	Expect(err).To(HaveOccurred())
}

func TestCreateClient(t *testing.T) {
	RegisterTestingT(t)
	c, err := New("some-user-id", "some-api-token")
	Expect(err).ToNot(HaveOccurred())
	Expect(c).ToNot(BeNil())
}

func TestConfigure_DefaultBaseUrl(t *testing.T) {
	RegisterTestingT(t)
	c, err := New("user", "api")
	Expect(err).ToNot(HaveOccurred())
	Expect(c.BaseURL).To(Equal("https://habitica.com/api/v3"))
}

func TestConfigure_BaseUrl(t *testing.T) {
	RegisterTestingT(t)
	c, err := New(
		"user",
		"api",
		WithBaseURL("https://somethingelse.com"),
	)
	Expect(err).ToNot(HaveOccurred())
	Expect(c.BaseURL).To(Equal("https://somethingelse.com"))
}

func TestConfigure_DefaultUserAgent(t *testing.T) {
	RegisterTestingT(t)
	c, err := New("user", "api")
	Expect(err).ToNot(HaveOccurred())
	Expect(c.UserAgent).To(Equal("go-habitica/1"))
}

func TestConfigure_DefaultHttpClient(t *testing.T) {
	RegisterTestingT(t)
	c, err := New("user", "api")
	Expect(err).ToNot(HaveOccurred())
	Expect(c.Client).ToNot(BeNil())
}

func TestConfigure_HttpClient(t *testing.T) {
	RegisterTestingT(t)
	hc := &http.Client{Timeout: time.Second}
	c, err := New(
		"user",
		"api",
		WithHttpClient(hc),
	)
	Expect(err).ToNot(HaveOccurred())
	Expect(c.Client).To(Equal(hc))
}

func TestConfigure_TaskService(t *testing.T) {
	RegisterTestingT(t)
	_, err := New("user", "api")
	Expect(err).To(BeNil())
}

func TestNewRequest_CorrectHeaders(t *testing.T) {
	RegisterTestingT(t)
	c, err := New("user", "api")
	Expect(err).ToNot(HaveOccurred())
	urlPath := "tasks/group/some-group-id"
	request, err := c.NewRequest(http.MethodGet, urlPath, nil)
	Expect(err).ToNot(HaveOccurred())
	Expect(request.Method).To(Equal(http.MethodGet))
	Expect(request.UserAgent()).To(Equal(UserAgent))
	Expect(request.URL.String()).To(Equal(fmt.Sprintf("%s/%s", c.BaseURL, urlPath)))
	Expect(request.Header.Get("x-api-user")).To(Equal("user"))
	Expect(request.Header.Get("x-api-key")).To(Equal("api"))
	Expect(request.Header.Get("Content-Type")).To(Equal("application/json"))
}

func TestNewRequest_ErrorForBadMethod(t *testing.T) {
	RegisterTestingT(t)
	c, err := New("user", "api")
	Expect(err).ToNot(HaveOccurred())
	_, err = c.NewRequest(" GOT", "", nil)
	Expect(err).To(HaveOccurred())
}
