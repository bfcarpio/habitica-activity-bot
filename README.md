# habitica-acitivity-bot

Habitica bot (hopefully) that computes activity statistics for a guild

## Mentions
+ I blatantly tool the `habitica` package from [wfernandes/go-habitica](https://github.com/wfernandes/go-habitica). It's [unlicensed](https://github.com/wfernandes/go-habitica/blob/develop/LICENSE) and works well according to my light testing. However, it's been untouched for 2 years and I only wanted to base part of the package and didn't want to increase the binary size.

