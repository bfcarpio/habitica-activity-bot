// Main running package
package main

import (
	"context"
	"fmt"
	"github.com/buger/jsonparser"
	"github.com/namsral/flag"
	"gitlab.com/bfcarpio/habitica-activity-bot/habitica"
	"io/ioutil"
	"log"
	"sync"
	"time"
)

type groupAndRecentMember struct {
	guildID        string
	recentMemberID string
}

func (garm *groupAndRecentMember) url() string {
	if garm.recentMemberID == "" {
		return fmt.Sprintf("/groups/%s/members", garm.guildID)
	}
	return fmt.Sprintf("/groups/%s/members?lastId=%s", garm.guildID, garm.recentMemberID)
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}

func main() {
	var userID, token, guild string
	flag.StringVar(&userID, "u", "", "Habitica API user ID")
	flag.StringVar(&token, "t", "", "Habitica API token (LIKE A PASSWORD)")
	flag.StringVar(&guild, "g", "party", "Habitica guild being analysed")
	flag.Parse()

	c1 := make(chan string, 30)
	c2 := make(chan string, 30)
	c3 := make(chan map[string]int, 1)

	h, err := habitica.New(userID, token)
	check(err)

	s := fmt.Sprintf("/members/%s", userID)
	body, err := habiticaRequest(h, "GET", s, nil)
	check(err)

	name, err := jsonparser.GetString(body, "data", "profile", "name")
	check(err)
	log.Printf("Signed in as %s", name)

	go requestMemberData(h, c1, c2)
	go reduceToTimePeriods(h, c2, c3)

	garm := groupAndRecentMember{guild, ""}
	for pageCount := 30; pageCount == 30; {
		log.Println("Getting page")
		page, err := habiticaRequest(h, "GET", garm.url(), nil)
		if err != nil {
			log.Fatal(err)
			// Figure out how to retry
		}
		// go functions to get all IDs
		arySize := 0
		lastMemberID := ""
		jsonparser.ArrayEach(page, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			memberID, err := jsonparser.GetString(value, "id")
			c1 <- memberID

			// Iterables
			lastMemberID = memberID
			arySize++
		}, "data")
		pageCount = arySize
		garm.recentMemberID = lastMemberID

	}
	close(c1)

	res := <-c3
	log.Println(res)
}

func habiticaRequest(h *habitica.HabiticaClient, method, path string, body interface{}) ([]byte, error) {
	req, err := h.NewRequest(method, path, body)
	if err != nil {
		return nil, err
	}

	res, err := h.Do(context.TODO(), req)
	if err != nil {
		return nil, err
	}

	info, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return info, nil
}

func requestMemberData(h *habitica.HabiticaClient, c1 chan string, c2 chan string) error {
	var wg sync.WaitGroup
	for elem := range c1 {
		wg.Add(1)
		go func(e string) {
			url := fmt.Sprintf("/members/%s", e)
			body, err := habiticaRequest(h, "GET", url, nil)
			check(err)

			lastLoginTimeStamp, err := jsonparser.GetString(body, "data", "auth", "timestamps", "loggedin")
			check(err)

			log.Printf(lastLoginTimeStamp)
			c2 <- lastLoginTimeStamp
			wg.Done()
		}(elem)
	}
	wg.Wait()
	close(c2)
	return nil
}

func reduceToTimePeriods(h *habitica.HabiticaClient, c2 chan string, c3 chan map[string]int) error {
	now := time.Now()
	layout := "2006-01-02T15:04:05.000Z"
	acc := make(map[string]int)
	for elem := range c2 {
		log.Printf("Reducing %s", elem)
		t, err := time.Parse(layout, elem)
		check(err)

		elapsed := now.Sub(t)

		if elapsed.Hours() >= 24*30 {
			acc["overMonth"]++
		} else if elapsed.Hours() >= 24*7 {
			acc["inMonth"]++
		} else if elapsed.Hours() >= 24 {
			acc["inWeek"]++
		} else if elapsed.Hours() >= 12 {
			acc["overHalfDay"]++
		} else {
			acc["inHalfDay"]++
		}

		acc["count"]++
	}
	log.Println("Sending Map")
	c3 <- acc
	close(c3)
	return nil
}
